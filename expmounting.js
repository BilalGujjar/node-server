const express = require("express");
const { update } = require("lodash");
const app = express();
app.use(express.json());
app.listen(4000);


let users = [
  {
    name: "Bilal",
    id: 1,
  },
  {
    name: "Mohsin",
    id: 2,
  },
  {
    name: "Abdullah",
    id: 3,
  },
  {
    name: "Usman",
    id: 4,
  },
];


const userRouter = express.Router();

app.use("/user", userRouter);

userRouter
  .route("/")
  .get(getPost)
  .post(getPost)
  .patch(getUpdate)
  .delete(doDelete);
userRouter.route("/:id")
.get(getUser);


function getUser (req,res){
  res.send(users)
}

function getPost(req, res) {
  console.log(req.body);
  users = req.body;
  res.json({
    message: "Data Is Up on Server",
    user: req.body,
  });
}
function getUpdate(req, res) {
  console.log("req data",req.body);
  let dataupdate = req.body;
   
  for (key in dataupdate) {
    users[key] = dataupdate[key];
  }
  res.json({
    message: "Data Is Update",
  });
}
function doDelete(req, res) {
  users = {};
  res.send({
    message: "Data Delete UP",
  });
}
function getID(req, res) {
  console.log(req.params.username);
  console.log(req.params);
  res.send("User is Liten");
}
