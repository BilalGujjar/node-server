const express =require('express')
const server =express()
server.listen(3000);
server.get('/', (req, res) => {
    res.send('Hello World!')
  }) 
  server.get('/pic', (req, res) => {
    res.sendFile('./pic.png',{root:__dirname})
  }) 
  server.get('/in', (req, res) => {
    res.sendFile('./index.html',{root:__dirname})
  }) 
  
  server.get('/rd', (req, res) => {
    res.sendFile('./data.json',{root:__dirname})
  })
  server.get('/hit', (req, res) => {
    res.status=301
    res.redirect('/rd')
  }) 
server.use((req,res)=>{
    res.status(404).sendFile('./404.html',{root:__dirname})
})