const http = require("http");
const fs = require("fs");

const server = http.createServer((req, res) => {
  let path = "./";

  switch (req.url) {
    case "/view":
      path = "index.html";
      break;
    case "/me":
      path = "about.html";
      break;
    case "/hit":
      res.statusCode = 301;
      res.setHeader("Location", "/pic");
      res.end();

      break;
    case "/pic":
      path = "pic.png";
      res.statusCode=200
      break;
    default:
      path = "404.html";
      break;
  }
  fs.readFile(path, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      res.end(data);
    }
  });
});

server.listen(3200, "localhost", () => {
  console.log("Live Server on Port 3200");
});
